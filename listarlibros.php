<?php
    include 'conexion/conexion.php';
    $query = "SELECT * FROM libros";
    $consulta_libros= $conexion->query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'includes/head.php' ?>
    <title>Document</title>
</head>
<body>
    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered" id="usuarios">
                <thead>
                    <tr>
                        <th scope="col">Id Libro</th>
                        <th scope="col">Titulo Libro</th>
                        <th scope="col">Autor Libro</th>
                        <th scope="col">Ano de Publicacion</th>
                        <th scope="col">Idioma</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_libros->num_rows > 0){
                            while($libros = $consulta_libros->fetch_assoc()){
                    ?>
                                <tr>
                                    <td> <?php echo $libros['idLibro'] ?> </td>
                                    <td> <?php echo $libros['tituloLibro'] ?> </td>
                                    <td> <?php echo $libros['autorLibro'] ?> </td>
                                    <td> <?php echo $libros['anopublicacionLibro'] ?> </td>
                                    <td> <?php echo $libros['idiomaLibro'] ?> </td>
                                    
                                        
                                    </td>
                                </tr>
                    <?php    }
                        } 
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php include 'includes/scripts.php' ?>
</body>
</html>
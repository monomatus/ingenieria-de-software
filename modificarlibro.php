<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'includes/head.php' ?>
    <title>Modificar Libros</title>
</head>
<body>
    <div class="contenedor">
        <div class="titulo">
            <h3>Modificar Libros</h3>
            <hr>
        </div>
            
                
        <form action="modificarLibro_form.php" method="GET">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Ingrese Nombre de Libro" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-block" style="background-color: #2597CB; color:white">Aceptar</button>  
                </div>
                <div class="col-md-3">
                    <a href="index.php" class="btn btn-block" style="background: #2597CB; color: white">Volver</a>
                </div>
            </div>
        </form>
    </div>
    <?php require 'includes/scripts.php' ?>
</body>
</html>
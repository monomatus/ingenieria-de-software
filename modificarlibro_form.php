<?php
    include 'conexion/conexion.php';

    $nombre = $_GET['nombre'];

    $consulta = "SELECT * FROM Libros WHERE tituloLibro='$nombre'";
    $resultado = $conexion->query($consulta);

    $fila = $resultado->fetch_assoc();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'includes/head.php' ?>
    <title>Modificar Libro</title>
</head>
<body>
    
    <div class="contenedor">
        <div class="titulo">
            <h3>Modificar Libro</h3>
            <hr>
        </div>
        <div class="cuerpo">
            <form action="modificar-libro.php" method="POST">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Id del libro:</span>
                            <input type="number" class="form-control" value="<?php echo $fila['idLibro']?>" id="idLibro" name="idLibro">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Titulo del libro:</span>
                            <input type="text" class="form-control" value="<?php echo $fila['tituloLibro']?>" id="tituloLibro" name="tituloLibro">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Autor del libro:</span>
                            <input type="text" class="form-control" value="<?php echo $fila['autorLibro']?>" id="autorLibro" name="autorLibro">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Año de publicación:</span>
                            <input type="number" class="form-control" value="<?php echo $fila['anopublicacionLibro']?> "id="anopublicacionLibro" name="anopublicacionLibro">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Idioma del libro:</span>
                            <input type="text" class="form-control" value="<?php echo $fila['idiomaLibro']?>" id="idiomaLibro" name="idiomaLibro">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-block" style="background-color: #2597CB; color:white">Modificar</button>  
                    </div>
                    <div class="col-md-6">
                        <a href="modificarlibro.php" class="btn btn-block" style="background: #2597CB; color: white">Volver</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <?php require 'includes/scripts.php' ?>
</body>
</html>
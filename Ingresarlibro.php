<?php
     include 'conexion/conexion.php';



?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'includes/head.php' ?>
    <title>Ingresar Libro</title>
</head>
<body>
    <div class="contenedor">
        <div>
            <h3>Ingresar libro</h3>
            <form action="agregarlibro.php" method="GET">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Id del libro:</span>
                            <input type="number" class="form-control" id="idLibro" name="idLibro">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Titulo del libro:</span>
                            <input type="text" class="form-control" id="tituloLibro" name="tituloLibro">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Autor del libro:</span>
                            <input type="text" class="form-control" id="autorLibro" name="autorLibro">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Año de publicación:</span>
                            <input type="number" class="form-control" id="anopublicacionLibro" name="anopublicacionLibro">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>Idioma del libro:</span>
                            <input type="text" class="form-control" id="idiomaLibro" name="idiomaLibro">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-block" style="background: #2597CB; color: white">Añadir</button>
                </div>
            </form>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <?php 
                    if(!empty($_GET['error'])){
                        $respuesta = $_GET['error'];      
                        $contenido = $_GET['contenido'];                                           
                ?>    
                <?php if($respuesta =='vacio'){
                ?>
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <?php echo $contenido; ?>
                        </div>
                    </div>    
                <?php
                    }else{
                ?>
                        <div class="alert alert-danger">
                            <?php echo $contenido ?>
                        </div>
                <?php
                        }
                    }    
                ?>
            </div>
             
        
        </div>



    </div>

    

        <?php include "includes/scripts.php"?>
</body>
</html>

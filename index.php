<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'includes/head.php' ?>
    <title>Libro</title>
</head>
<body>
    <div class="contenedor">
        <div class="titulo">
            <h3>Esta aplicacion es para Gestionar Libros</h3>
            <hr>
        </div>
        <div class="cuerpo">
        <table class="table table-bordered" id="usuarios">
                <thead>
                    <tr>
                        <th scope="col">
                            <form action="ingresarlibro.php">
                                <button class="btn btn-block" style="background: #2597CB; color: white">Agregar Libro </button>
                            </form>    
                        </th>
                        <th scope="col">
                            <form action="modificarlibro.php">   
                                <button class="btn btn-block" style="background: #2597CB; color: white">Modificar Libro </button>
                            </form> 
                        </th>
                        <th scope="col">
                            <form action="listarlibros.php">
                                <button class="btn btn-block" style="background: #2597CB; color: white">Listar Libros </button>
                            </form>    
                        </th>
                        <th scope="col">
                            <form action="eliminarlibro.php">
                                <button class="btn btn-block" style="background: #2597CB; color: white">Eliminar Libro </button>
                            </form>
                        </th>

                    </tr>
                </thead>
                <tbody>
        </table>

        <div class="row">
            <div class="col-md-12">
                <?php
                    if(!empty($_GET['error'])){
                        $r = $_GET['error'];
                        $c = $_GET['contenido'];
                ?>
                    <?php 
                        if($r == 'vacio'){ 
                    ?>
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    <?php 
                                        echo $c;
                                    ?>
                                </div>
                            </div>
                    <?php 
                        }else if($r == 'modificado'){ 
                    ?>
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    <?php 
                                        echo $c;
                                    ?>
                                </div>
                            </div>    
                    <?php 
                        }
                    ?>
                    
                <?php
                    } 
                ?>
                    
                            </div>
                        </div>
                <?php        
                    
                ?>
            </div>
        </div>
            
        </div>    


    </div>
    <?php include 'includes/scripts.php' ?>
</body>
</html>

